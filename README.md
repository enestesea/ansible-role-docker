# Ansible Role Docker

## Role Name
docker

## Requirements

## Role Variables

## Dependencies
None
## Example Playbook

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

- hosts: all
  roles:
     - docker

## License
BSD

## Author Information
Arteezy and Nastia
